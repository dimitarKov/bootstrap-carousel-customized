# Custom carousel with react bootstrap

## Props

### slideVariant - accepts integer (1-4)
- Changes slide structure and attaches corresponding css styles and/or bootstrap classes:
  - 1: slide is one row
  - 2: slide is a grid with four columns (rows are added automatically depending on the data)
  - 3: slide is a grid with three columns (rows are added automatically depending on the data)
  - 4: slide is one row with rounded images

### chunkSize - accepts integer
- Divides array data into smaller chunks (sub-arrays) and fills each slide correspondingly.
  - Example: chunkSize={1} fills first slide with all data, chungSize={2} splits data in two chunks and fills two slides correspondingly etc.

### imgSrc - accepts array
 **Important!** slideVariant 1-3 accept array of strings, slideVariant 4 accept array of objects.
