import './App.css';
import CustomCarousel from './components/Carousel/CustomCarousel';

const imgSrcPartners = ['alkaloid', 'cocacola', 'kozuvcanka', 'makpetrol', 'nike', 'pivaraskopje'];

const cardData = [{
  id: 1,
  title: 'Lorem1',
  description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi eius cupiditate aspernatur ea, vitae iusto!',
  img: 'karticka1.png',
  date: '19.12.2021'
}, {
  id: 2,
  title: 'Lorem2',
  description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi eius cupiditate aspernatur ea, vitae iusto!',
  img: 'karticka2.png',
  date: '19.12.2021'
}, {
  id: 3,
  title: 'Lorem3',
  description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi eius cupiditate aspernatur ea, vitae iusto!',
  img: 'karticka3.png',
  date: '19.12.2021'
}, {
  id: 4,
  title: 'Lorem4',
  description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi eius cupiditate aspernatur ea, vitae iusto!',
  img: 'karticka1.png',
  date: '19.12.2021'
}, {
  id: 5,
  title: 'Lorem5',
  description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi eius cupiditate aspernatur ea, vitae iusto!',
  img: 'karticka2.png',
  date: '19.12.2021'
}, {
  id: 6,
  title: 'Lorem6',
  description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi eius cupiditate aspernatur ea, vitae iusto!',
  img: 'karticka3.png',
  date: '19.12.2021'
}];

const imgSrcPartners2 = ['boa', 'gallagher', 'metLife', 'brightshot', 'golisano', 'pg', 'ms', 'safilo', 'ikea', 'td', 'mp', 'ccola'];

const sportsImgSrc = [{ id: 1, imgSrc: 'basketball', title: 'кошарка' }, { id: 2, imgSrc: 'football', title: 'фудбал' }, { id: 3, imgSrc: 'cycling', title: 'велосипедизам' }, { id: 4, imgSrc: 'dancing', title: 'танцување' }, { id: 5, imgSrc: 'athletics', title: 'атлетика' }, { id: 6, imgSrc: 'floorball', title: 'флор бол' }, { id: 7, imgSrc: 'swimming', title: 'пливање' }, { id: 8, imgSrc: 'exercises', title: 'телесни вежби' }];

function App() {
  return (
    <div className="App">
      <CustomCarousel imgSrc={imgSrcPartners} chunkSize={2} slideVariant={1} />
      <br />
      <CustomCarousel imgSrc={imgSrcPartners2} chunkSize={1} slideVariant={2} />
      <br />
      <CustomCarousel imgSrc={cardData} chunkSize={1} slideVariant={3} />
      <br />
      <CustomCarousel imgSrc={sportsImgSrc} chunkSize={2} slideVariant={4} />
    </div>
  );
}

export default App;
