import React from 'react';
import './Card.css';

export default function Card({ id, title, description, img, date }) {
    return (
        <div className='card-custom' key={id}>
            <img src={require(`../../img/${img}`)} alt="" className='d-block w-100' />
            <div className='custom-card-body'>
                <div className="custom-card-title">
                    <h4>{title}</h4>
                    <span>{date}</span>
                </div>
                <p>{description}</p>
            </div>
        </div>
    )
}