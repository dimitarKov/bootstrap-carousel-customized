import React, { Component } from 'react';
import './CustomCarousel.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/js/dist/carousel.js';
import Carousel from 'react-bootstrap/Carousel';
import Card from '../Card/Card';

export default class CustomCarousel extends Component {
    groupItems(arr, chunk_size) {
        let output = new Array(chunk_size);
        for (let i = 0; i < chunk_size; i++) {
            output[i] = [];
        }
        arr.forEach((el, idx) => {
            output[idx % chunk_size].push(el);
        });
        return output;
    }

    render() {
        const imgSrc = this.props.imgSrc;
        const chunkSize = this.props.chunkSize;
        const slideVariant = this.props.slideVariant;
        return (
            <Carousel variant="dark" indicators={slideVariant === 1 || 4 ? false : true} controls={slideVariant === 1 || 4 ? true : false} className={`partners-carousel ${slideVariant === 3 && 'indicators-minus-margin'}`}>
                {this.groupItems(imgSrc, chunkSize).map(element => (
                    <Carousel.Item key={slideVariant === 4 ? element.map(e => e.id) : element}>
                        <div className={slideVariant === 2 ? 'carousel-item-grid grid-4' : (slideVariant === 3) ? 'carousel-item-grid grid-3' : undefined}>
                            {element.map(item => (
                                (slideVariant === 1 || slideVariant === 2) ?
                                    <div className="wrapper" key={element.indexOf(item) + (item)} style={{ width: `calc(100% / ${chunkSize})` }}>
                                        {/* note to self: don't forget to change src with the corresponding server url !!! */}
                                        <img src={require(`../../img/${item}.png`)} alt="" />
                                    </div> :
                                    (slideVariant === 4)
                                        ?
                                        <div className="wrapper position-relative text-center" key={element.indexOf(item) + item.title} style={{ width: `calc(100% / ${chunkSize})` }}>
                                            <img src={require(`../../img/${item.imgSrc}.png`)} className='img-fluid rounded-filter' alt="" />
                                            <h5 className='sport-heading'>{item.title}</h5>
                                        </div>
                                        :
                                        <Card key={item.id} id={item.id} title={item.title} description={item.description} img={item.img} date={item.date} />
                            ))}
                        </div>
                    </Carousel.Item>
                ))}
            </Carousel>
        )
    }
}